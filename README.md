# Buzzlogix Social Media Monitor Frontend #


## How to run locally ##

just cd into project and run the application with:

development: `meteor run --settings settings.json`

production: `meteor run --settings settings.json --production`

This will grab the necessary packages, bundle all the css and js and start the application

Go to http://<hostname>:3000 to see live version


## Deploy to AppEngine ##

Firstly ensure that you have two gcloud configurations, one called buzzlogix-dev and the other called buzzlogix-prod.
These can be set by running gcloud init. The projects will be switched between automatically when deploying to live or
dev.

### Set the version: ###

Set the version for the upload in package.json in the deploy script.

### Deploying to Buzzlogix-Development ###

Ensure that you are on the development branch, then run:

`npm run deploy:dev`

### Deploying to Buzzlogix-Production ###

Ensure that you are on the master branch, then run:

`npm run deploy:prod`