#!/usr/bin/env bash
export PORT=3000
export MONGO_URL=mongodb://localhost:27017/myapp
export METEOR_SETTINGS=$(cat bundle/settings.json)
export ROOT_URL=http://104.197.85.110
pm2 start bundle/main.js