Meteor.publish('timelineGenericPostsPub', function (streamId, filters, size, timeZone, loc) {
    var self = this;

    try {
        if (buildTimelineQuery(filters) == null || buildTimelineQuery(filters) == '') {
            var apiUrl = Meteor.settings.backendURL + '/generic_post/read?streamId=' + streamId;
        } else {
            var apiUrl = Meteor.settings.backendURL + '/generic_post/read?streamId=' + streamId + '&filter=' + buildTimelineQuery(filters);
        }

        apiUrl += '&size=' + size;

        var response = Meteor.wrapAsync(apiCallGet)(apiUrl);
        
        var sourceIcon, sourceColor, sourceText;
        var saTwitter;
        timeZone = timeZone || 'Europe/London';


        _.each(response, function (item) {
            switch (item.source) {
                case 'TWITTER':
                    sourceIcon = 'fa-twitter';
                    sourceColor = '#1da1f2';
                    saTwitter = true;
                    sourceText = "Twitter";
                    break;
                case 'NEWS':
                    sourceIcon = 'fa-newspaper-o';
                    sourceColor = '#F7C300';
                    saTwitter = false;
                    sourceText = "News";
                    break;
                case 'BLOGS':
                    sourceIcon = 'fa-newspaper-o';
                    sourceColor = 'orange';
                    saTwitter = false;
                    sourceText = "Blogs";
                    break;
                case 'DISCUSSIONS':
                    sourceIcon = 'fa-rss';
                    sourceColor = 'green';
                    saTwitter = false;
                    sourceText = "Forums";
                    break;
                default:
                    sourceIcon = 'fa-bullhorn';
                    sourceColor = '#1da1f2';
                    saTwitter = false;
                    sourceText = "NaN";
                    break;
            }

            if (item.location == "null") {
                var loc = 'The Universe';
            } else {
                var loc = item.location;
            }


            var extl = '';
            var imagesArr = [];
            if (typeof (item.externalLinks) != 'undefined') {
                extl = item.externalLinks;
                _s.ltrim(extl, ['"']);
                _s.rtrim(extl, ['"']);
                extl.replace('\\"', '"');
                imagesArr = JSON.parse(extl);
            }

            //convertion date to unix time stamp
            var published1 = item.published / 1000;

            var post = {
                id: item.id,
                imgMain: item.imgMain || null,
                language: getLanguageName(item.language) || null,
                location: loc,
                //conversion date to log time (13 digits)
                published: moment(published1 * 1000).format('LLLL'),
                sourceIcon: sourceIcon,
                source: sourceText,
                streamId: item.streamId,
                text: item.text,
                title: item.title || strTruncate(item.text, 60, '...'),
                subtitle: '',
                url: item.url,
                externalImage: imagesArr[0] || null,
                dateDiff: moment(published1 * 1000).fromNow(),
                saTwitter: saTwitter,
                saColor: sourceColor
            };
            self.added('timelineGenericPosts', Random.id(), post);
        });

        self.ready();
    } catch (error) {
        console.log(error);
    }
});

Meteor.publish('timelineStreamsPub', function (userid) {
    var self = this;
    try {
        var apiUrl = Meteor.settings.backendURL + '/account/read?id=' + userid;
        var response = Meteor.wrapAsync(apiCallGet)(apiUrl);
        var stream = [];

        _.each(response.groups[0].topics, function (item) {
            stream = {
                id: item.id,
                name: item.streamName
            };

            self.added('timelineStreams', Random.id(), stream);
        });
        self.ready();


    } catch (error) {
        console.log(error);
    }
});

