/* global Meteor, Email */

Meteor.methods({
    'getLocations': function () {
        try {
            var apiUrl = Meteor.settings.backendURL + '/location/read-all';
            var response = Meteor.wrapAsync(apiCallGet)(apiUrl);

            return response;
        } catch (error) {
            console.log(error);
        }
    },
    'updateUser': function (user) {
        try {
            var apiUrl = Meteor.settings.backendURL + '/user/update';
            var response = Meteor.wrapAsync(apiCallPost)(apiUrl, user);

            return response;
        } catch (error) {
            console.log(error);
        }
    },
    'sendEmail': function (to, subject, body) {
        Email.send({
            to: to,
            from: "GAP Domain <postmaster@sandbox2987743652d84827ac6bbd68ee35eacc.mailgun.org>",
            subject: subject,
            html: body
        });
    },
    'actionStream': function (action, stream) {
        try {
            var apiUrl = Meteor.settings.backendURL + '/topic/' + action;
            var response = Meteor.wrapAsync(apiCallPost)(apiUrl, stream);

            return response;
        } catch (error) {
            console.log(error);
        }
    },
    'authUser': function (userid, pass) {
        var self = this;
        self.unblock();

        try {

            var apiUrl = Meteor.settings.backendURL + '/user/login?username=' + userid + '&password=' + pass;
            var response = Meteor.wrapAsync(apiCallGetHeaders)(apiUrl);

            return response;

        } catch (error) {
            console.log(error);
        }
    },
    'authUserExistEmail': function (email) {
        var self = this;
        self.unblock();

        try {

            var apiUrl = Meteor.settings.backendURL + '/user/login-exist-email?email=' + email;
            var response = Meteor.wrapAsync(apiCallGet)(apiUrl);

            return response;
        } catch (error) {
            console.log(error);
        }
    },
    'authUserExist': function (username, email) {
        var self = this;
        self.unblock();

        try {

            var apiUrl = Meteor.settings.backendURL + '/user/login-exist?username=' + username + '&email=' + email;
            var response = Meteor.wrapAsync(apiCallGet)(apiUrl);

            return response;
        } catch (error) {
            console.log(error);
        }
    },
    'getUserAccount': function (userid) {
        var self = this;
        self.unblock();

        try {

            var apiUrl = Meteor.settings.backendURL + '/account/read?id=' + userid;
            var response = Meteor.wrapAsync(apiCallGet)(apiUrl);

            return response;

        } catch (error) {
            console.log(error);
        }
    },
    'getGenericPosts': function (streamId, filters) {
        var self = this;
        self.unblock();
        try {
            var apiUrl = Meteor.settings.backendURL + '/generic_post/read?streamId=' + streamId;
            var response = Meteor.wrapAsync(apiCallGet)(apiUrl);
            return response;
        } catch (error) {
            console.log(error);
        }
    },
    'putStream': function (streamData) {
        var self = this;
        self.unblock();

        // Build JSON
        var dataObj = {};

        dataObj['id'] = streamData['groupId'];
        dataObj['topics'] = [];
        dataObj['topics'][0] = {};
        dataObj['topics'][0]['id'] = streamData['id'];
        dataObj['topics'][0]['streamName'] = streamData['name'];
        dataObj['topics'][0]['createdBy'] = streamData['username'];
        dataObj['topics'][0]['updatedBy'] = streamData['username'];
        dataObj['topics'][0]['topicConfig'] = {};
        dataObj['topics'][0]['topicConfig']['sources'] = streamData['sources'];
        dataObj['topics'][0]['topicConfig']['allTerms'] = streamData['terms'];
        dataObj['topics'][0]['topicConfig']['poolTime'] = streamData['poolTime'];
        dataObj['topics'][0]['blackList'] = {};
        dataObj['topics'][0]['blackList']['name'] = 'BlackList';
        dataObj['topics'][0]['blackList']['value'] = '@EurojackpotNews,@footynews129,@SenateDFL,- Bevorstehende Geschäftsergebnisse -';
        dataObj['topics'][0]['tag'] = {};
        dataObj['topics'][0]['tag']['name'] = 'DFL Corporate';
        dataObj['topics'][0]['tag']['value'] = ["Deutsche Fußball Liga", "DFL", "Ligaverband", "Andreas Rettig", "Peter Peters", "Glückspielstattsvertrag", "@bundesliga_de", "Deutsche Fußball-Liga"];

        try {
            var apiUrl = Meteor.settings.backendURL + '/group/update';
            var response = Meteor.wrapAsync(apiCallPost)(apiUrl, dataObj);

            return response;
        } catch (error) {
            console.log(error);
        }
    },
    'updateStream': function (streamData) {
        var self = this;
        self.unblock();

        // Build JSON
        var dataObj = {};
        dataObj['id'] = streamData['id'];
        dataObj['streamName'] = streamData['name'];
        dataObj['updatedBy'] = streamData['username'];
        dataObj['topicConfig'] = {};
        dataObj['topicConfig']['sources'] = streamData['sources'];
        dataObj['topicConfig']['allTerms'] = [];
        dataObj['topicConfig']['allTerms'][0] = streamData['terms'];
        /*dataObj['blackList'] = {};
         dataObj['blackList']['name'] = 'BlackList';
         dataObj['blackList']['value'] = '@EurojackpotNews,@footynews129,@SenateDFL,- Bevorstehende Geschäftsergebnisse -';
         dataObj['tag'] = {};
         dataObj['tag']['name'] = 'DFL Corporate';
         dataObj['tag']['value'] = ["Deutsche Fußball Liga", "DFL", "Ligaverband", "Andreas Rettig", "Peter Peters", "Glückspielstattsvertrag", "@bundesliga_de", "Deutsche Fußball-Liga"];
         */

        //console.log(dataObj);

        try {
            var apiUrl = Meteor.settings.backendURL + '/topic/update';
            var response = Meteor.wrapAsync(apiCallPost)(apiUrl, dataObj);
            //console.log(response);
            return response;
        } catch (error) {
            console.log(error);
        }
    },
    'deleteStream': function (streamId) {
        var self = this;
        self.unblock();


        try {
            var apiUrl = Meteor.settings.backendURL + '/topic/delete?id=' + streamId;
            var response = Meteor.wrapAsync(apiCallSimplePost)(apiUrl);

            return response;
        } catch (error) {
            console.log(error);
        }
    },
    'addUser': function (userData) {
        var self = this;
        self.unblock();


        try {
            var apiUrl = Meteor.settings.backendURL + '/add/admin';
            var response = Meteor.wrapAsync(apiCallPost)(apiUrl, userData);

            return response;
        } catch (error) {
            console.log(error);
        }
    },
    'addSubuser': function (group) {
        var self = this;
        self.unblock();


        try {
            var apiUrl = Meteor.settings.backendURL + '/group/update';
            var response = Meteor.wrapAsync(apiCallPost)(apiUrl, group);

            return response;
        } catch (error) {
            console.log(error);
        }
    },
    'getPostCount': function (streamid) {
        var self = this;
        self.unblock();


        try {
            var apiUrl = Meteor.settings.backendURL + '/generic_post/count?streamId=' + streamid;
            var response = Meteor.wrapAsync(apiCallGet)(apiUrl);

            return response;
        } catch (error) {
            console.log(error);
        }
    },
    'getStreamInfo': function (useruuid) {
        Meteor.call('getUserAccount', useruuid, function (err, resg) {
            if (err) {
                console.log(err.reason);
            } else {
                if (resg == null) {
                    console.log("Error Login");
                } else {

                    return resg;
                }
            }
        });
    }

});

apiCallGet = function (apiUrl, callback) {
    // try…catch allows you to handle errors
    try {
        console.log('*** API CALL URL: ' + apiUrl);
        var response = HTTP.get(apiUrl, {timeout: Meteor.settings.restApiTimeout}).data;
        // A successful API call returns no error
        // but the contents from the JSON response
        callback(null, response);
    } catch (error) {
        // If the API responded with an error message and a payload
        if (error.response) {
            var errorCode = error.response.data.code;

            var errorMessage = error.response.data.message;
            // Otherwise use a generic error message
        } else {
            var errorCode = 500;
            var errorMessage = 'Cannot access the API';
        }
        // Create an Error object and return it via callback
        var myError = new Meteor.Error(errorCode, errorMessage);
        callback(myError, null);
    }
};

apiCallPost = function (apiUrl, postData, callback) {
    try {
        Meteor.http.call('POST', apiUrl,
                {
                    data: postData,
                    headers: {'content-type': 'application/json'}
                }, function (error, result) {
            if (error) {
                console.log(error);
            } else {
                callback(null, result.content);
            }
        });

    } catch (error) {
        // If the API responded with an error message and a payload
        if (error.response) {
            var errorCode = error.response.data.code;
            var errorMessage = error.response.data.message;
            // Otherwise use a generic error message
        } else {
            var errorCode = 500;
            var errorMessage = 'Cannot access the API';
        }
        // Create an Error object and return it via callback
        var myError = new Meteor.Error(errorCode, errorMessage);
        callback(myError, null);
    }
};

apiCallSimplePost = function (apiUrl, callback) {
    try {
        Meteor.http.call('POST', apiUrl, function (error, result) {
            if (error) {
                console.log(error);
            } else {
                callback(null, result.content);
            }
        });

    } catch (error) {
        // If the API responded with an error message and a payload
        if (error.response) {
            var errorCode = error.response.data.code;
            var errorMessage = error.response.data.message;
            // Otherwise use a generic error message
        } else {
            var errorCode = 500;
            var errorMessage = 'Cannot access the API';
        }
        // Create an Error object and return it via callback
        var myError = new Meteor.Error(errorCode, errorMessage);
        callback(myError, null);
    }
};

getStreamTotal = function (streamId) {
    Meteor.call('getPostCount', streamId, function (err, res) {
        if (err) {
            console.log(err.reason);
        } else {
            if (res == null) {
                console.log("Error in count");
            } else {
                return res.total;
            }
        }
    });
};

apiCallGetHeaders = function (apiUrl, callback) {
    // try…catch allows you to handle errors
    try {
        console.log('*** API CALL AND HEADERS URL: ' + apiUrl);
        var response = HTTP.get(apiUrl, {timeout: Meteor.settings.restApiTimeout});
        // A successful API call returns no error
        // but the contents from the JSON response
        callback(null, response);
    } catch (error) {
        // If the API responded with an error message and a payload
        if (error.response) {
            var errorCode = error.response.data.code;

            var errorMessage = error.response.data.message;
            // Otherwise use a generic error message
        } else {
            var errorCode = 500;
            var errorMessage = 'Cannot access the API';
        }
        // Create an Error object and return it via callback
        var myError = new Meteor.Error(errorCode, errorMessage);
        callback(myError, null);
    }
};