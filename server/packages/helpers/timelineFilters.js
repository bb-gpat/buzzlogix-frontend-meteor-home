buildTimelineQuery = function (filters) {

    q = [];
    if (filters.language != null) {
        q.push('language IN ' + obj2json(filters.language));
    }

    if (filters.source != null) {
        q.push('source IN ' + obj2json(filters.source));
    }

    if (filters.country != null) {
        q.push('location IN ' + obj2json(filters.country));
    }

    if (q != []) {
        return encodeURI(q.join(' AND '));
    } else {
        return null;
    }

};

function obj2json(_data) {
    var str = '(';
    var first = true;
    _.each(_data, function (i) {
        if (first != true)
            str += ",";
        else first = false;

        str += '\'' + i + '\'';
    });
    return str += ')';
}

function fixedEncodeURIComponent(str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function (c) {
        return '%' + c.charCodeAt(0).toString(16);
    });
}