function registerUser() {
    var userdata = {
        title: $('select.title').val(),
        name: $('input[name=name]').val(),
        surname: $('input[name=surname]').val(),
        email: $('input[name=email]').val(),
        username: $('input[name=username]').val(),
        password: $('input[name=password]').val(),
        account: {
            name: $('input[name=company]').val()
                    // groups : [
                    //     {
                    //         name: $('input[name=group]').val()
                    //     }
                    // ]
        }
    };

    Meteor.call('addUser', userdata, function (error, response) {
        if (response == 'null') {
            swal({
                title: "This user is already registered!",
                text: "Please proceed to login.",
                type: "warning"
            }, function () {
                Router.go("login");
            });
        } else {
            swal({
                title: "Registration Successful!",
                text: "Please proceed to login.",
                type: "success"
            }, function () {
                Router.go("login");
            });
        }
    });
}

Template.register.rendered = function () {
    $("#form").validate({
        rules: {
            name: {
                required: true,
                minlength: 3
            },
            surname: {
                required: true,
                minlength: 3
            },
            company: {
                required: true,
                minlength: 3
            },
            username: {
                required: true,
                minlength: 3
            },
            password: {
                required: true,
                minlength: 3
            },
            confirm: {
                required: true,
                minlength: 3,
                equalTo: '#password'
            }
        },
        messages: {
            email: {
                required: "Custom message for required",
                email: "email address is invalid"
            }
        },
        submitHandler: function (form) {
            registerUser();
        }
    });
};

Template.register.helpers({
    title: function() {
      return [ "Ms", "Mrs" ];
    },
    language: function () {
        var language = [
            {
                value: "sq",
                name: "Albania"
            },
            {
                value: "ar",
                name: "Arabic"
            },
            {
                value: "be",
                name: "Belarusian"
            },
            {
                value: "bg",
                name: "Bulgarian"
            },
            {
                value: "ca",
                name: "Catalan"
            },
            {
                value: "zh",
                name: "Chinese"
            },
            {
                value: "hr",
                name: "Croatian"
            },
            {
                value: "cs",
                name: "Czech"
            },
            {
                value: "da",
                name: "Danish"
            },
            {
                value: "nl",
                name: "Dutch"
            },
            {
                value: "en",
                name: "English"
            },
            {
                value: "et",
                name: "Estonian"
            },
            {
                value: "fi",
                name: "Finnish"
            },
            {
                value: "fr",
                name: "French"
            },
            {
                value: "de",
                name: "German"
            },
            {
                value: "el",
                name: "Greek"
            },
            {
                value: "iw",
                name: "Hebrew"
            },
            {
                value: "hi",
                name: "Hindi"
            },
            {
                value: "hu",
                name: "Hungarian"
            },
            {
                value: "is",
                name: "Icelandic"
            },
            {
                value: "in",
                name: "Indonesian"
            },
            {
                value: "ga",
                name: "Irish"
            },
            {
                value: "it",
                name: "Italian"
            },
            {
                value: "ja",
                name: "Japanese"
            },
            {
                value: "ko",
                name: "Korean"
            },
            {
                value: "lv",
                name: "Latvian"
            },
            {
                value: "lt",
                name: "Lithuanian"
            },
            {
                value: "ms",
                name: "Malay"
            },
            {
                value: "mt",
                name: "Maltese"
            },
            {
                value: "no",
                name: "Norwegian"
            },
            {
                value: "pl",
                name: "Polish"
            },
            {
                value: "pt",
                name: "Portuguese"
            },
            {
                value: "ro",
                name: "Romanian"
            },
            {
                value: "ru",
                name: "Russian"
            },
            {
                value: "sr",
                name: "Serbian"
            },
            {
                value: "sk",
                name: "Slovak"
            },
            {
                value: "sl",
                name: "Slovenian"
            },
            {
                value: "es",
                name: "Spanish"
            },
            {
                value: "sv",
                name: "Swedish"
            },
            {
                value: "th",
                name: "Thai"
            },
            {
                value: "tr",
                name: "Turkish"
            },
            {
                value: "uk",
                name: "Ukrainian"
            },
            {
                value: "vi",
                name: "Vietnamese"
            }
        ];

        return language;
    },
    location: function () {
        Meteor.call('getLocations', function (error, response) {
            if (error) {
                console.log(error.reason);
            } else {
                Session.set('locations', response);
            }
        });

        return Session.get('locations');
    }
});