Template.createUser.helpers({
    'company': function () {
        var company = Session.get('clientID').account.name;

        return company;
    }
});

Template.createUser.events({
    'submit form': function (event, template) {
        event.preventDefault();

        var group = {
            id: Session.get('clientID').account.groups[0].id,
            users: [{
                    id: Meteor.uuid(),
                    title: $('select.title').val(),
                    name: $('input[name=name]').val(),
                    surname: $('input[name=surname]').val(),
                    email: $('input[name=email]').val(),
                    username: $('input[name=username]').val(),
                    password: $('input[name=password]').val(),
                    role: $('select[name=role]').val(),
                    accountId: Session.get('clientID').account.id
                }]
        }

        Meteor.call('addSubuser', group, function (error, response) {
            if (response == 'null') {
                swal({
                    title: "This user is already registered!",
                    text: "Please try again.",
                    type: "warning"
                }, function () {
                    Router.go("dashboard");
                });
            } else {
                swal({
                    title: "Create user Successful!",
                    text: "",
                    type: "success"
                }, function () {
                    var userid = Session.get('clientID').username;
                    var email = Session.get('clientID').email;
                    
                    console.log(userid);
                    console.log(email);

                    Meteor.call('authUserExist', userid, email, function (err, res) {
                        if (err) {
                            console.log(err.reason);
                        } else {
                            if (res == null) {
                                console.log("Error Login");
                            } else {
                                Session.setPersistent('clientID', res);
                                Router.go("dashboard");
                            }
                        }
                    });
                    Router.go("dashboard");
                });
            }
        });
    }
});