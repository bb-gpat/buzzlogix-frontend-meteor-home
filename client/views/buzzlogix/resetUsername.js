/* global Template, Router, Meteor, Session */

Template.resetUsername.events({
    'submit form': function (event, template) {
        event.preventDefault();

        var id = Router.current().params.query.id;
        var username = $('input[name=username]').val();
        var password = $('input[name=password]').val();
        var confirmPassword = $('input[name=confirmPassword]').val();

        if (password !== confirmPassword) {
        } else {
            var user = {
                id: id,
                username: username,
                password: password
            };

            Meteor.call('updateUser', user, function (error, res) {
                if (error !== null) {
                    console.log(error);
                } else {
                    Session.setPersistent('clientID', res);
                }
            });
        }
    }
});