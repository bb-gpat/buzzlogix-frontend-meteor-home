TimelinePosts = new Meteor.Collection('timelineGenericPosts');
TimelineStreams = new Meteor.Collection('timelineStreams');
Streams = new Meteor.Collection(null);
TimelineFilters = {};
filterObj = {};
newStream = true;
var ITEMS_INCREMENT = 30;

TimelineStreams.allow({
    insert: function () {
        return true;
    },
    update: function () {
        return true;
    }
});
Streams.allow({
    insert: function () {
        return true;
    },
    update: function () {
        return true;
    }
});

Template.listenViewTimeline.onCreated(function () {
    var self = this;
    var currentStream = '';

    Session.clear('timelineFilters');
    Session.setPersistent('timelinePage', 30);

    self.autorun(function () {
        Meteor.subscribe('timelineStreamsPub', Session.get('clientID').account.id, Session.get('timelinePage'), {
            onReady: function () {
                currentStream = TimelineStreams.findOne()['id'];
                Session.setPersistent('currentStream', currentStream);
                getStreamTotal(Session.get('currentStream'));
                Session.setPersistent('timelineFilters', filterObj);

            }

        });



        Meteor.subscribe('timelineGenericPostsPub', Session.get('currentStream'), Session.get('timelineFilters'), Session.get('timelinePage'), Session.get('clientID').timeZone, Session.get('locale'), {
            onReady: function () {
                console.log('subscribe: ' + newStream);

                if (newStream == true) {
                    getStreamTotal(Session.get('currentStream'));
                    newStream = false;
                }
                $('#showMoreResults').css('display', 'none');
                $('.timelineItemText').each(function () {
                    $(this).readmore({
                        collapsedHeight: 80,
                        speed: 75,
                        moreLink: '<a class="btn btn-ssm btn-primary" href="#" role="button"><i class="fa fa-arrow-circle-o-down"></i> Read more</a>',
                        lessLink: '<a class="btn btn-ssm btn-primary" href="#" role="button"><i class="fa fa-arrow-circle-o-up"></i> Read less</a>'
                    });
                });
                $('img').onerror = function () {
                    $(this).remove();
                };
                $(window).on('scroll', showMoreVisible);
            }
        });
    });

});

Template.listenViewTimeline.events({
    'change .streamSelector': function (event) {
        var newStreamId = $(event.target).val();
        var oldStreamId = Session.get('currentStream');

        if (newStreamId != oldStreamId) {
            console.log('Stream changed to: ' + $(event.target).val());
            Session.update('currentStream', newStreamId);
            newStream = true;
            //TimelineStreams.update('SELECTED', {selected: event.target.value});
        }
    },
    'change .filtersSelect': function () {

        var sourceArr = $('select.filtersSource').val();
        var countryArr = $('select.countrySource').val();
        var tagsArr = $('select.tagsSource').val();
        var languageArr = $('select.filtersLanguage').val();

        filterObj = {
            source: sourceArr,
            country: countryArr,
            language: languageArr,
            tags: tagsArr
        };

        Session.update('timelineFilters', filterObj);


    },
    'click .refreshStreamBtn': function () {
        $('.timelineItemText').remove();

        Meteor.subscribe('timelineGenericPostsPub', Session.get('currentStream'), Session.get('timelineFilters'), Session.get('timelinePage'), {
            onReady: function () {
                Meteor.call('getPostCount', Session.get('currentStream'), function (err, res) {
                    if (err) {
                        console.log(err.reason);
                    } else {
                        if (res == null) {
                            console.log("Error in count");
                        } else {
                            //console.log('Total: ' + res.total);
                            Session.setPersistent('streamTotal', res.total);
                        }
                    }
                });
                $('#showMoreResults').css('display', 'none');
                $('.timelineItemText').each(function () {
                    $(this).readmore({
                        collapsedHeight: 80,
                        speed: 75,
                        moreLink: '<a class="btn btn-ssm btn-primary" href="#" role="button"><i class="fa fa-arrow-circle-o-down"></i> Read more</a>',
                        lessLink: '<a class="btn btn-ssm btn-primary" href="#" role="button"><i class="fa fa-arrow-circle-o-up"></i> Read less</a>'
                    });
                });
                $('img').onerror = function () {
                    $(this).remove();
                };
                $(window).on('scroll', showMoreVisible);
            }
        });
    },
    'click .deleteStream': function () {
        var streamId = $('.streamSelector').val();
        var streamName = $('.streamSelector option:selected').text();
        if (streamId == Session.get('currentStream')) {
            swal({
                title: "Are you sure?",
                text: "Are you sure you want to delete this stream? You will not be able to recover it",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (isConfirm) {
                    Meteor.call('deleteStream', streamId, function (err, res) {
                        if (err) {
                            console.log(err.reason);
                            swal("Error", streamName + "is not deleted!", "error");
                        } else {
                            if (res == null) {
                                console.log("Error in delete");
                            } else {
                                $('.streamSelector').html("");
                                Meteor.subscribe('timelineStreamsPub', Session.get('clientID').account.id, Session.get('timelinePage'), {
                                    onReady: function () {
                                        currentStream = TimelineStreams.findOne()['id'];
                                        Session.setPersistent('currentStream', currentStream);
                                        Session.setPersistent('timelineFilters', filterObj);
                                    }

                                });
                                swal("Deleted!", streamName + " has been deleted.", "success");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", streamName + "is safe :)", "error");

                }

            });
        }

    },
    'change .selectSentiment': function (event) {
        var face;

        switch (event.target.value) {
            case 'neutral':
                face = 'meh';
                break;
            case 'positive':
                face = 'smile';
                break;
            case 'negative':
                face = 'frown';
                break;
        }
        var facecss = 'fa fa-' + face + '-o';
        $(event.currentTarget).parent('.input-group ').find('.input-group-addon').find('i').removeClass().addClass(facecss);
    },
    'click .addNewStream': function () {
        $('.modal').modal('toggle');
    },
    'click .refreshStream': function () {
        console.log('refresh stream: ' + $(event.target).val());

        newStream = true;
    },
    'click .editStream': function () {

        var useruuid = Session.get('clientID').account.id;
        Meteor.call('getUserAccount', useruuid, function (err, res) {

            if (err) {
                console.log(err.reason);
            } else {
                if (res == null) {
                    console.log("Error Login");
                } else {
                    topics = res['groups'][0].topics;

                    _.each(topics, function (topicItem) {

                        topic = {
                            id: topicItem.id,
                            streamName: topicItem.streamName,
                            topicConfig: topicItem.topicConfig,
                            sources: topicItem.topicConfig.sources,
                            term: topicItem.topicConfig.allTerms[0],
                            blackList: topicItem.blackList,
                            tag: topicItem.tag
                        };

                        Streams.insert(topic);

                    });

                    var stream = Streams.findOne({id: Session.get('currentStream')});
                    Session.set('streamdata', stream);
                    $('#editStream input[name="streamName"]').val(stream.streamName);
                    $('#editStream input[name="streamTerms"]').val(stream.term);
                    _.each(stream.sources, function (item) {
                        $('#editStream input[name="streamSources"][value="' + item + '"]').attr('checked', 'checked');
                    })
                }
            }
        });
    }
});

Template.listenViewTimeline.helpers({
    timelinePosts: function () {
        return TimelinePosts.find();
    },
    timelineStreams: function () {
        return TimelineStreams.find();
    },
    editStream: function () {
        return Session.get('streamdata');
    }
});


Template.listenViewTimeline.rendered = function () {
    checkActions();

    // Set special clsss to wraper to add  margin for right sidebar
    $('#page-wrapper').addClass('sidebar-content');
    $('.modalN').appendTo("body");
    $('.modalE').appendTo("body");

    // Set the full height of right sidebar
    var heightWithoutNavbar = $("body > #wrapper").height() - 61;
    $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

    /*$('.i-checks').iCheck({
     checkboxClass: 'icheckbox_square-green',
     radioClass: 'iradio_square-green'
     });*/

    /*$('.timelineItemText').each(function() {
     console.log( $( this ).text() );
     $(this).readmore({
     speed: 75,
     lessLink: '<a href="#">Read less</a>'
     });
     });*/

    $('.refreshStreamBtn').on('click')


    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    };

    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    var sourcesArray = [];

    $("#wizard").steps({
        onFinished: function (event, currentIndex)
        {

            sourcesArray = $("input[name='streamSources']:checked").map(function () {
                return this.value;
            }).get();

            var streamId = Meteor.uuid();
            var stream = {};

            stream.name = $('input[name=streamName]').val();
            stream.id = streamId;
            stream.sources = sourcesArray;
            stream.terms = [$('input[name=streamTerms]').val()];
            stream.location = $('input[name=streamLocation]').val();
            stream.poolTime = $('input[name=streamPoolTime]').val();
            stream.groupId = Session.get('clientGroupId');
            stream.username = Session.get('clientID').name + ' ' + Session.get('clientID').surname;

            Meteor.call('putStream', stream, function (err, res) {
                if (err) {
                    console.log(err.reason);
                } else {
                    if (res == 'null') {
                        swal({
                            title: "Error in new Stream!",
                            text: "",
                            type: "error"
                        }, function () {
                            $('.modal').modal('toggle');
                        });
                    } else {
                        swal({
                            title: "Stream created successfully!",
                            text: "",
                            type: "success"
                        }, function () {
                            $('.modal').modal('toggle');
                            $('.streamSelector').html("");
                            Meteor.subscribe('timelineStreamsPub', Session.get('clientID').account.id, Session.get('timelinePage'), {
                                onReady: function () {
                                    console.log(res);
                                    currentStream = TimelineStreams.findOne()['id'];
                                    Session.setPersistent('currentStream', streamId);
                                    Session.setPersistent('timelineFilters', filterObj);
                                    $('.streamSelector').val(streamId);
                                }

                            });
                        });
                    }
                }
            });
        }
    });
    $("#wizardEdit").steps({
        onFinished: function (event, currentIndex)
        {

            sourcesArray = $("input[name='streamSources']:checked").map(function () {
                return this.value;
            }).get();

            var streamId = Session.get('streamdata').id;
            var stream = {};

            stream.name = $('#editStream input[name=streamName]').val();
            stream.id = streamId;
            stream.sources = sourcesArray;
            stream.terms = $('#editStream input[name=streamTerms]').val();
            stream.location = $('#editStream input[name=streamLocation]').val();
            stream.username = Session.get('clientID').name + ' ' + Session.get('clientID').surname;
            Meteor.call('updateStream', stream, function (err, res) {
                //console.log(res);
                if (err) {
                    console.log(err.reason);
                } else {
                    if (res == 'null') {
                        swal({
                            title: "Error in new Stream!",
                            text: "",
                            type: "error"
                        }, function () {
                            $('.modal').modal('toggle');
                        });
                    } else {
                        swal({
                            title: "Stream updated successfully!",
                            text: "",
                            type: "success"
                        }, function () {
                            $('.modal').modal.hide();
                            Meteor.subscribe('timelineStreamsPub', Session.get('clientID').account.id, Session.get('timelinePage'), {
                                onReady: function () {
                                    currentStream = Session.get('streamdata');
                                    Session.setPersistent('timelineFilters', filterObj);
                                }

                            });
                        });
                    }
                }
            });
        }
    });


};


Template.listenViewTimeline.destroyed = function () {
    $('#page-wrapper').removeClass('sidebar-content');
};


function showMoreVisible() {
    var target = $("#showMoreResults");

    if (!target.length)
        return;

    if (Session.get('streamTotal') >= ITEMS_INCREMENT) {
        if ($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
            $(window).unbind('scroll');
            if (!target.data("visible")) {
                target.data("visible", true);
                target.css('display', 'block');
                Session.set("timelinePage", Session.get('timelinePage') + ITEMS_INCREMENT);
            }
        } else {
            if (target.data("visible")) {
                target.css('display', 'none');
                target.data("visible", false);
            }
        }
    }

}


getStreamTotal = function (streamId) {
    Meteor.call('getPostCount', streamId, function (err, res) {
        if (err) {
            console.log(err.reason);
        } else {
            if (res == null) {
                console.log("Error in count");
            } else {
                ///console.log('Total: ' + res.total);
                Session.setPersistent('streamTotal', res.total);
            }
        }
    });
};


function checkActions() {
    if (Session.get("clientID").role == "ADMIN" || Session.get("clientID").role == "EDITOR") {
        $('#actions').show();
    } else {
        $('#actions').hide();
    }
}