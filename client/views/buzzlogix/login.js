/* global Template, Meteor, Session, Router */

Template.login.events({
    'submit form': function (event, template) {
        event.preventDefault();
        var userid = $('input[name=userid]').val();
        var pass = $('input[name=password]').val();
        Meteor.call('authUser', userid, pass, function (err, res) {
            if (err) {
                console.log(err.reason);
            } else {
                if (res.data == null) {
                    swal({
                        title: "The username and password you entered don't match.",
//                        text: "Please proceed to login.",
                        type: "warning"
                    }, function () {
                        Router.go("login");
                    });
                } else {
                    Session.setPersistent('clientID', res.data);
                    Session.setPersistent('clientGroupId', res.data.account.groups[0].id);
                    Router.go("dashboard");
                }
            }
        });
    }
});