/* global Template, Session, Meteor */

function actionStream(action, streamId) {
    var topics = Session.get('clientID').account.groups[0].topics;

    if (topics !== null) {
        topics.forEach(function (topic, index, arr) {
            if (topic.id === streamId) {
                Meteor.call('actionStream', action, topic, function (error, response) {
                    if (response !== 'null') {
                        var userid = Session.get('clientID').username;
                        var email = Session.get('clientID').email;

                        console.log(userid);
                        console.log(email);

                        Meteor.call('authUserExist', userid, email, function (err, res) {
                            if (err) {
                                console.log(err.reason);
                            } else {
                                if (res === 'null') {
                                    console.log("Error Login");
                                } else {
                                    Session.setPersistent('clientID', res);
                                }
                            }
                        });
                    }
                });

                return;
            }
        });
    }
}

Template.profile.events({
    'change .actionStream': function (event) {
        var info = $(event.target).val().split('_');

        if (info !== null) {
            actionStream(info[0], info[1]);
        }

        $(event.target).val(-1);
    },
    'submit form': function () {
        var name = $('input[type=name]').val();
        var surname = $('input[type=surname]').val();
        var email = $('input[type=email]').val();
        
        var user = {
            id: Session.get('clientID').id,
            name : name,
            surname : surname,
            email : email
        };
        
        Meteor.call('updateUser', user, function (error, res) {
            if (error !== null) {
                console.log(error);
            } else {
                Session.setPersistent('clientID', res);
            }
        });
    }
});


Template.profile.rendered = function () {
    initialDataTable();
    checkTabInfo();
};

Template.profile.helpers({
    'title': function () {
        return Session.get('clientID').title;
    },
    'name': function () {
        return Session.get('clientID').name;
    },
    'surname': function () {
        return Session.get('clientID').surname;
    },
    'email': function () {
        return Session.get('clientID').email;
    },
    'language': function () {
        return Session.get('clientID').language;
    },
    'timeZone': function () {
        return Session.get('clientID').timeZone;
    },
    'username': function () {
        return Session.get('clientID').username;
    },
    'accountName': function () {
        return Session.get('clientID').account.name;
    },
    'licenceStarted': function () {
        return Session.get('clientID').account.billing.licence.started;
    },
    'licenceExpires': function () {
        return Session.get('clientID').account.billing.licence.expires;
    },
    'limitMaxSocials': function () {
        return Session.get('clientID').account.billing.limit.maxSocials;
    },
    'limitMaxStreams': function () {
        return Session.get('clientID').account.billing.limit.maxStreams;
    },
    'limitMaxUsers': function () {
        return Session.get('clientID').account.billing.limit.maxUsers;
    },
    'users': function () {
        return getUsers();
    },
    'topics': function () {
        return getTopics();
    }
});

function initialDataTable() {
    $('.dataTables-example').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},
            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
    });
}

function checkTabInfo() {
    if (Session.get('clientID').role === "EDITOR") {
        $('li.users').hide();
    } else if (Session.get('clientID').role === "VIEWER") {
        $('li.users').hide();
        $('li.stream').hide();
    }
}

function getUsers() {
    var account = Session.get('clientID').account;

    if (account) {
        var groups = account.groups;

        if (groups) {
            var users = groups[0].users;

            if (users) {
                return users;
            }
        }
    }
}

function getTopics() {
    var account = Session.get('clientID').account;

    if (account) {
        var groups = account.groups;

        if (groups) {
            var topics = groups[0].topics;

            if (topics) {
                return topics;
            }
        }
    }
}