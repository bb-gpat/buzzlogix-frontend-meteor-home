/* global Template, Email, Router, Meteor */

Template.forgotUsername.events({
    'submit form': function (event, template) {
        event.preventDefault();

        var email = $('input[name=email]').val();

        if (email !== null) {
            Meteor.call('authUserExistEmail', email, function (err, res) {
                if (err) {
                    console.log(err.reason);
                } else {
                    if (res !== null) {
                        var subject = "Reset your buzzlogix username";
                        var body = "Hello " + res.title + " " + res.name + " " + res.surname + ", <p>Follow this <a href='" + window.location.origin + "/resetUsername?id='" + res.id +
                                ">link</a> to activate your account.</p>";

                        Meteor.call('sendEmail', email, subject, body, function (err, res) {
                            if (err) {
                                console.log(err.reason);
                            } else {
                                swal({
                                    title: "The username reset email, containing a reset link has been sent to your email (" + email + ")",
                                    type: "success"
                                }, function () {
                                    Router.go("login");
                                });
                            }
                        });
                    } else {
                        swal({
                            title: "The username doesn't exist (" + email + ")",
                            type: "warning"
                        });
                    }
                }
            });
        }
    }
});