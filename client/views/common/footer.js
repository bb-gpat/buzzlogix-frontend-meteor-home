Template.footer.rendered = function () {

    // FIXED FOOTER
    // Uncomment this if you want to have fixed footer or add 'fixed' class to footer element in html code
    $('.footer').addClass('fixed');

};

Template.footer.helpers ({
    tier: function () {
        if(Meteor.isDevelopment) {
            return "Development";
        } else {
            return "Production";
        }
    }
});