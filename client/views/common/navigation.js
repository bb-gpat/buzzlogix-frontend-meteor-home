Template.navigation.rendered = function () {

    // Initialize metisMenu
    $('#side-menu').metisMenu();

    if (Session.get('clientID').role != "ADMIN") {
        $('li.createUser').hide();
    }
};

// Used only on OffCanvas layout
Template.navigation.events({

    'click .close-canvas-menu': function () {
        $('body').toggleClass("mini-navbar");
    }

});

Template.navigation.helpers({

    'name': function () {
        return Session.get('clientID').name;
    },
    'surname': function () {
        return Session.get('clientID').surname;
    },
    'company': function () {
        return Session.get('clientID').account.name;
    },
    'devEnv': function () {
        Meteor.isDevelopment = (Meteor.isServer ? process.env.ROOT_URL : window.location.origin).indexOf('localhost') != -1;
        
        return Meteor.isDevelopment;

    }

});